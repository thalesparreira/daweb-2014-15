# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Status.create([
{status:"Paid"},
{status:"Cancelled"},
{status:"Unknown"}]);

Item.create([
{desc:"Pencil"},
{desc:"Binder"},
{desc:"Pen"},
{desc:"Desk"},
{desc:"Pen Set"}
]);

Representative.create([
 {name:"Jones"},
 {name:"Kivell"},
 {name:"Jardine"},
 {name:"Gill"},
 {name:"Sorvino"},
 {name:"Andrews"},
 {name:"Thompson"},
 {name:"Morgan"},
 {name:"Howard"},
 {name:"Parent"},
 {name:"Smith"}
 ]);

Region.create([
 {name:"East"},
 {name:"Central"},
 {name:"West"}
]);

Order.create([
{region_id:1,orderdate:"1/6/10",item_id:1,status_id:1,discount:0,units:95,cost:199,rep_id:1},
{region_id:2,orderdate:"1/23/10",item_id:2,status_id:1,discount:5,units:50,cost:1999,rep_id:2},
{region_id:2,orderdate:"2/9/10",item_id:1,status_id:1,discount:0,units:36,cost:499,rep_id:3},
{region_id:2,orderdate:"2/26/10",item_id:3,status_id:2,discount:0,units:27,cost:1999,rep_id:4},
{region_id:3,orderdate:"3/15/10",item_id:1,status_id:1,discount:10,units:56,cost:299,rep_id:5},
{region_id:1,orderdate:"4/1/10",item_id:2,status_id:2,discount:10,units:60,cost:499,rep_id:1},
{region_id:2,orderdate:"4/18/10",item_id:1,status_id:1,discount:0,units:75,cost:199,rep_id:6},
{region_id:2,orderdate:"5/5/10",item_id:1,status_id:1,discount:0,units:90,cost:499,rep_id:3},
{region_id:3,orderdate:"5/22/10",item_id:1,status_id:1,discount:0,units:32,cost:199,rep_id:7},
{region_id:1,orderdate:"6/8/10",item_id:2,status_id:1,discount:5,units:60,cost:899,rep_id:1},
{region_id:2,orderdate:"6/25/10",item_id:1,status_id:1,discount:0,units:90,cost:499,rep_id:8},
{region_id:1,orderdate:"7/12/10",item_id:2,status_id:2,discount:0,units:29,cost:199,rep_id:9},
{region_id:1,orderdate:"7/29/10",item_id:2,status_id:1,discount:0,units:81,cost:1999,rep_id:10},
{region_id:1,orderdate:"8/15/10",item_id:1,status_id:1,discount:0,units:35,cost:499,rep_id:1},
{region_id:2,orderdate:"9/1/10",item_id:4,status_id:1,discount:10,units:2,cost:12500,rep_id:11},
{region_id:1,orderdate:"9/18/10",item_id:5,status_id:1,discount:0,units:16,cost:1599,rep_id:1},
{region_id:2,orderdate:"10/5/10",item_id:2,status_id:1,discount:0,units:28,cost:899,rep_id:8},
{region_id:1,orderdate:"10/22/10",item_id:3,status_id:1,discount:5,units:64,cost:899,rep_id:1},
{region_id:1,orderdate:"11/8/10",item_id:3,status_id:1,discount:0,units:15,cost:1999,rep_id:10},
{region_id:2,orderdate:"11/25/10",item_id:5,status_id:1,discount:0,units:96,cost:499,rep_id:2},
{region_id:2,orderdate:"12/12/10",item_id:1,status_id:1,discount:0,units:67,cost:129,rep_id:11},
{region_id:1,orderdate:"12/29/10",item_id:5,status_id:2,discount:0,units:74,cost:1599,rep_id:10},
{region_id:2,orderdate:"1/15/11",item_id:2,status_id:1,discount:0,units:46,cost:899,rep_id:4},
{region_id:2,orderdate:"2/1/11",item_id:2,status_id:1,discount:0,units:87,cost:1500,rep_id:11},
{region_id:1,orderdate:"2/18/11",item_id:2,status_id:1,discount:0,units:4,cost:499,rep_id:1},
{region_id:3,orderdate:"3/7/11",item_id:2,status_id:1,discount:10,units:7,cost:1999,rep_id:5},
{region_id:2,orderdate:"3/24/11",item_id:5,status_id:1,discount:0,units:50,cost:499,rep_id:3},
{region_id:2,orderdate:"4/10/11",item_id:1,status_id:1,discount:10,units:66,cost:199,rep_id:6},
{region_id:1,orderdate:"4/27/11",item_id:3,status_id:2,discount:0,units:96,cost:499,rep_id:9},
{region_id:2,orderdate:"5/14/11",item_id:1,status_id:1,discount:0,units:53,cost:129,rep_id:4},
{region_id:2,orderdate:"5/31/11",item_id:2,status_id:2,discount:0,units:80,cost:899,rep_id:4},
{region_id:2,orderdate:"6/17/11",item_id:4,status_id:1,discount:0,units:5,cost:12500,rep_id:2},
{region_id:1,orderdate:"7/4/11",item_id:5,status_id:1,discount:0,units:62,cost:499,rep_id:1},
{region_id:2,orderdate:"7/21/11",item_id:5,status_id:1,discount:0,units:55,cost:1249,rep_id:8},
{region_id:2,orderdate:"8/7/11",item_id:5,status_id:1,discount:0,units:42,cost:2395,rep_id:2},
{region_id:3,orderdate:"8/24/11",item_id:4,status_id:1,discount:0,units:3,cost:27500,rep_id:5},
{region_id:2,orderdate:"9/10/11",item_id:1,status_id:1,discount:10,units:7,cost:129,rep_id:4},
{region_id:3,orderdate:"9/27/11",item_id:3,status_id:1,discount:1,units:76,cost:199,rep_id:5},
{region_id:3,orderdate:"10/14/11",item_id:2,status_id:1,discount:0,units:57,cost:1999,rep_id:7},
{region_id:2,orderdate:"10/31/11",item_id:1,status_id:1,discount:0,units:14,cost:129,rep_id:6},
{region_id:2,orderdate:"11/17/11",item_id:2,status_id:1,discount:0,units:11,cost:499,rep_id:3},
{region_id:2,orderdate:"12/4/11",item_id:2,status_id:1,discount:0,units:94,cost:1999,rep_id:3},
{region_id:2,orderdate:"12/21/11",item_id:2,status_id:2,discount:10,units:28,cost:499,rep_id:6},
{region_id:2,orderdate:"8/7/12",item_id:5,status_id:1,discount:10,units:50,cost:2395,rep_id:2},
{region_id:3,orderdate:"8/24/12",item_id:4,status_id:1,discount:0,units:3,cost:27500,rep_id:5},
{region_id:2,orderdate:"9/10/13",item_id:1,status_id:1,discount:10,units:7,cost:129,rep_id:4},
{region_id:3,orderdate:"9/27/12",item_id:3,status_id:1,discount:0,units:76,cost:199,rep_id:5},
{region_id:3,orderdate:"10/14/12",item_id:2,status_id:1,discount:0,units:17,cost:1999,rep_id:7},
{region_id:2,orderdate:"10/31/12",item_id:1,status_id:1,discount:0,units:13,cost:129,rep_id:6},
{region_id:2,orderdate:"11/17/12",item_id:2,status_id:1,discount:0,units:11,cost:499,rep_id:3},
{region_id:2,orderdate:"12/4/13",item_id:2,status_id:1,discount:0,units:94,cost:1999,rep_id:3},
{region_id:2,orderdate:"12/21/13",item_id:2,status_id:2,discount:15,units:28,cost:499,rep_id:6}]);

