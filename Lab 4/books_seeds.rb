# Development of Web Applications (FCTUNL)
# João Costa Seco
# 
# Seed data for the books example, thanks to Rui Carvalho


hem = Author.create(name: 'Ernest Hemingway')
dos = Author.create(name: 'Fyodor Dostoyevsky')
alb = Author.create(name: 'António Lobo Antunes')
mar = Author.create(name: 'Javier Marías')

pen = Publisher.create(name: 'Penguin')
qix = Publisher.create(name: 'Dom Quixote')
dbs = Publisher.create(name: 'Debolsillo')
mod = Publisher.create(name: 'Modern Library')

Book.create(title: 'Crime and Punishment', author: dos, publisher: pen, isbn: '0140449132', pages: '656')
Book.create(title: 'A Morte de Carlos Gardel', author: alb, publisher: qix, isbn: '9789722034081', pages: '336')
Book.create(title: 'Anna Karenina', author: dos, publisher: mod, isbn: '067978330X', pages: '976')
Book.create(title: 'Mañana en la batalla piensa en mí', author: mar, publisher: dbs, isbn: '9788483461723', pages: '352')
Book.create(title: 'To Have and Have Not', author:hem, publisher: pen, isbn: '0684818981', pages:'272')