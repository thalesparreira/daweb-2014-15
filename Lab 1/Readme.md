# Lab 1: Basic RESTful interface using a simple model

The goal is to define a basic web server implementing a basic RESTful interface of a small (not persistent) library database. You may find the pdf file with the lab assignment and a proposed solution written in NODE.js (folder books). The NODE.js code is modularized in three components: the web server, the routing function and the model representation. 

## Generic implementation of a model

The presented code has generic code for a resource, that only requires the creation of an entry in the database (a javascript object), and the declaration of the fields in each object (model.js).