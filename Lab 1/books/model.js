var util = require("util");
var qs = require("querystring");

function root(response) {
  var rcs = '';
  for ( r in resources ) {
    rcs += '<li><a href="/'+r+'">'+resources[r].label+'</a></li>';
  }
  response.writeHead(200, {"Content-Type": "text/html"});
    response.write(
      '<html>'+
    '<head><meta http-equiv="Content-Type" '+ 'content="text/html; charset=UTF-8" /></head>'+
    '<body>'+
    '<h1>Resources available</h1><ul>'+
    rcs+
    '</ul>'+
    '</body>'+
      '</html>'
  ); 
    response.end();
}

// PRE: method == POST
function createResource(request,response) {
  var resource = this;
  var info = '';
    request.on('data', function (data) { info += data; });
    request.on('end', function () {
      var element = resource.fillElement(qs.parse(info))
      element['id'] = db[resource.name].length;
      db[resource.name].push(element);

      response.writeHead(303, {"Location": "/"+resource.name});
    response.end();
    });
}

// PRE: method == GET
function listResource(response) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.write(util.inspect(db[this.name])); 
  response.end();
}

// PRE: method == POST
function updateResource(id,request,response) {
  var resource = this;
  var info = '';
    request.on('data', function (data) { info += data; });
    request.on('end', function () {
      var element = resource.fillElement(qs.parse(info))
      element['id'] = id;
      db[resource.name][indexOf(resource.name, id)] = element;

      response.writeHead(303, {"Location": "/"+resource.name});
    response.end();
    });
}

function indexOf(resourceName,id) {
  for( b in db[resourceName] ) {
    if (db[resourceName][b].id == id ) return b;
  }
}

function getById(resourceName,id) {
  return db[resourceName][indexOf(resourceName,id)];
}

function findById(id,response) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  for( b in db[this.name] ) {
    if (db[this.name][b].id == id ) {
      response.write(util.inspect(db[this.name][b])); 
      break;
    }
  }
    response.end(); 
}

function fillElement(values) {
  var element = {};
  for( f in this.fields ) {
    name = this.fields[f].name;
    element[name] = values[name];
  };
  return element;
}

function Resource(name,label) {
  // fields
  this.name = name;
  this.label = label;
  // REST Interface
  this.index = listResource;
  this.get = findById;
  this.create = createResource;
  this.update = updateResource;
  // form views
  this.new = formNew;
  this.edit = formEdit;
  // Auxiliary methods
  this.form = form;
  this.fillElement = fillElement;
}

function form(url, object, actionLabel, response) {
  var inputs = '';
  for( f in this.fields ) {
    var value = '';
    if( object[this.fields[f].name] != undefined )
      value = 'value = "'+object[this.fields[f].name]+'"';
    inputs += '<br>'+this.fields[f].label+':<input type="text" name="'+this.fields[f].name+'" cols="20" '+value+'></input>'
  }

  var body = '<html>'+
  '<head><meta http-equiv="Content-Type" '+ 'content="text/html; charset=UTF-8" /></head>'+
  '<body><form action="/'+url+'" method="post">'+
  inputs+
  '<div><input type="submit" value="'+actionLabel+' '+this.label+'" /></div>'+ 
  '</form></body></html>';

  response.writeHead(200, {"Content-Type": "text/html"});
  response.write(body);
  response.end();

}

function formNew(response) {
  this.form(this.name, {}, "New ", response);
}

function formEdit(object,response) {
  this.form(this.name+'/'+object.id, object, "Update", response);
}

var db = {
    "books": [
    {
       "id":0,
       "title": "Learning Node",
       "author": "Shelley Powers",
       "publisher": "O'Reilley",
       "isbn":"978-1-4493-2307-3"
    },
    {
       "id":1,
       "title": "What is Node?",
       "author": "Brett McLaughlin",
       "publisher": "O'Reilley",
       "isbn":"978-1-4493-1005-9"
    }],

    "authors": [
    {
    	"id" : 0,
    	"name" : "Brett McLaughlin"
    },
    {	
    	"id" : 1,
    	"name" : "Shelley Powers"
    } ],

    "publisher" : []
}

var resources = {}

resources["books"] = new Resource("books","Book");
resources["books"].fields = [{"label": "Title", "name":"title"},{"label": "Author", "name":"author"},{"label": "Publisher", "name":"publisher"},{"label": "ISBN", "name":"isbn"}];

resources["authors"] = new Resource("authors","Author");
resources["authors"].fields = [{"label": "Name", "name":"name"}];

resources["publisher"] = new Resource("publisher","Publisher");
resources["publisher"].fields = [{"label":"Name","name":"name"},{"label":"Address","name":"address"}]


exports.db = db
exports.resources = resources
exports.getById = getById


